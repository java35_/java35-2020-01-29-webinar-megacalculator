package v3;


public class CalculatorApp {

	public static void main(String[] args) {
		
		MenuItem menuItem1 = new MenuItemCalculator();
		MenuItem menuItem2 = new MenuItemExit();
		
		Menu menu = new Menu(menuItem1, menuItem2);
		menu.run();	
	}

}
