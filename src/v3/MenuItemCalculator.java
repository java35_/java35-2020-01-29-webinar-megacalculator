package v3;

public class MenuItemCalculator implements MenuItem {

	@Override
	public String getMenuItemName() {
		return "Calculator";
	}
	
	@Override
	public void execute() {
		InputOutput io = new InputOutputConsole();
		int n1 = io.inputInteger("Input number1");
		int n2 = io.inputInteger("Input number2");
		String op = io.inputString("Input operation (+, -, /, *)");
		
		switch (op) {
			case "+":
				System.out.println(n1 + n2);
				break;
			case "-":
				System.out.println(n1 - n2);
				break;
			case "*":
				System.out.println(n1 * n2);
				break;
			case "/":
				System.out.println((float)n1 / n2);
				break;
			default:
				System.out.println("Wrong operation!");
				break;
			}
	}
	
}
