package v3;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;

public interface InputOutput {
	
	public abstract String inputString(String prompt);
	
	default public Integer inputInteger(String prompt) {
//		Function<String, Integer> func = t -> Integer.parseInt(t);
//		Function<String, Integer> func = Integer::parseInt;
		return inputObject(prompt, "Wrong integer", Integer::parseInt);
	}
	
	default public Double inputDouble(String prompt) {
		return inputObject(prompt, "Wrong double", Double::parseDouble);
	}
	
	default public <R> R inputObject(String prompt, String errPrompt, 
										Function<String, R> func) {
		boolean wrong = true;
		while (wrong) {
			wrong = false;
			try {
				return func.apply(inputString(prompt));
			} catch (Exception e) {
				System.out.println(errPrompt);
				wrong = true;
			}
		};
		return null;
		BinaryOperator<T>
	}
	
	
}
