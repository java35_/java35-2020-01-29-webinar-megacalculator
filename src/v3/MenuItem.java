package v3;

public interface MenuItem {
	String getMenuItemName();
	default boolean isExit() {
		return false;
	}
	void execute();
}
