package v3;

import java.util.Scanner;

public class InputOutputConsole implements InputOutput {
	static Scanner scanner = new Scanner(System.in);
	
	public String inputString(String prompt) {
		boolean wrong = true;
		while (wrong) {
			wrong = false;
			try {
				System.out.println(prompt);
				return scanner.nextLine();
			} catch (Exception e) {
				wrong = true;
			}
		};
		return null;
	}
}
