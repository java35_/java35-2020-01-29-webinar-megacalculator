package v2;

import java.util.Scanner;

public class Menu {
	
	private MenuItem[] menuItems;
	
	public Menu(MenuItem ...menuItem) {
		if (menuItem == null || menuItem.length == 0)
			throw new IllegalArgumentException();
		this.menuItems = menuItem;
	}
	
	public void run() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Change number");
		for (int i = 0; i < menuItems.length; i++) {
			System.out.println((i + 1) + " " + menuItems[i].getMenuItemName());
		}
		
		// ������	|	���� ������������
		//   0		|			1          	- �����������
		//	 1		|			2			- ���� �������
		//	 2		|			3			- exit
		
		int userAnswer = Integer.parseInt(scanner.nextLine());
		
		// while (false) {
			// NOt work
		//}
		
		// while (true) {
			// work
		//}
		
		MenuItem menu_Item = menuItems[userAnswer - 1];
		boolean isItemMenuExit = menu_Item.isExit();
		
		while (!isItemMenuExit) {
//			execute
			menuItems[userAnswer - 1].execute();
			System.out.println("Change number");
			for (int i = 0; i < menuItems.length; i++) {
				System.out.println((i + 1) + " " + menuItems[i].getMenuItemName());
			}
			userAnswer = Integer.parseInt(scanner.nextLine());
			menu_Item = menuItems[userAnswer - 1];
			isItemMenuExit = menu_Item.isExit();
		}
	}
}
