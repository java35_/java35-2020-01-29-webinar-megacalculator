package v2;

public interface MenuItem {
	String getMenuItemName();
	default boolean isExit() {
		return false;
	}
	void execute();
}
