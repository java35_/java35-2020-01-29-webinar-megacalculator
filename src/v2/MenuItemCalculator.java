package v2;

import java.util.Scanner;

public class MenuItemCalculator implements MenuItem {

	@Override
	public String getMenuItemName() {
		return "Calculator";
	}
	
	@Override
	public void execute() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Input number1");
		int n1 = Integer.parseInt(scanner.nextLine());
		System.out.println("Input number2");
		int n2 = Integer.parseInt(scanner.nextLine());
		System.out.println("Input operation (+, -, /, *)");
		String op = scanner.nextLine();
		
		switch (op) {
			case "+":
				System.out.println(n1 + n2);
				break;
			case "-":
				System.out.println(n1 - n2);
				break;
			case "*":
				System.out.println(n1 * n2);
				break;
			case "/":
				System.out.println((float)n1 / n2);
				break;
			default:
				System.out.println("Wrong operation!");
				break;
			}
	}
	
}
