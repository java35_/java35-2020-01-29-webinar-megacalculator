package v1;

import java.util.Scanner;

public class CalculatorApp {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		// Change number 
		// 1 - Calculator
		// 2 - Exit
		
		// Calculator
		//  - number1
		//  - number2
		//  - operation
		
		System.out.println("Change number");
		System.out.println("1 - Calculator");
		System.out.println("2 - Exit");
		
		int userAnswer = Integer.parseInt(scanner.nextLine());
		
		while (userAnswer != 2) {
			
			if (userAnswer == 1) {
				System.out.println("Input number1");
				int n1 = Integer.parseInt(scanner.nextLine());
				System.out.println("Input number2");
				int n2 = Integer.parseInt(scanner.nextLine());
				System.out.println("Input operation (+, -, /, *)");
				String op = scanner.nextLine();
				
				switch (op) {
					case "+":
						System.out.println(n1 + n2);
						break;
					case "-":
						System.out.println(n1 - n2);
						break;
					case "*":
						System.out.println(n1 * n2);
						break;
					case "/":
						System.out.println((float)n1 / n2);
						break;
					default:
						System.out.println("Wrong operation!");
						break;
					}
			}
			System.out.println("Change number");
			System.out.println("1 - Calculator");
			System.out.println("2 - Exit");
			userAnswer = Integer.parseInt(scanner.nextLine());
		}
		
		System.out.println("����...");
		scanner.close();
	}

}
